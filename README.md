# CARD
CARD is a Free as in Freedom productivity system.

It's not like The Pomodoro Technique® or Getting Things Done™ by David Allen
even though the techniques of these systems are widely used, 
and I appreciate what these people have tried to do.

We need a Free\Libre system everyone can use adapt and build on.

CARD is supposed to be easy to use and easy to implement,
multi platform and free of copyright and trademarks,
your success is my gain.


CARD:
  - Capture - Everything ... in a system or app you trust
  - Action - Location - Tags - When and Where
  - Rank - Importance
  - Do - Pretty Obvious

The system can be used by most productivity applications.

Let's build on this and share your ideas!
## TaskWarrior using CARD
![Example](https://raw.githubusercontent.com/kmf/CARD/master/CARD-taskwarrior.jpg)

## todo.txt using CARD
![Example](https://raw.githubusercontent.com/kmf/CARD/master/CARD-todotxt.jpg)

## Plain Tasks for Sublime Text using CARD using CARD
![Example](https://raw.githubusercontent.com/kmf/CARD/master/CARD-plaintasks-for-sublime-text.jpg)

## Remember the Milk using CARD
![Example](https://raw.githubusercontent.com/kmf/CARD/master/CARD-RememberTheMilk.jpg)

## Todoist using CARD
![Example](https://raw.githubusercontent.com/kmf/CARD/master/CARD-Todoist.jpg)

## Google Tasks using CARD (lacks some Action features)
![Example](https://raw.githubusercontent.com/kmf/CARD/master/CARD-google-tasks.jpg)


